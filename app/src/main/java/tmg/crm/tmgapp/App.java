package tmg.crm.tmgapp;

import android.app.Application;

import tmg.crm.tmgapp.di.AppComponent;
import tmg.crm.tmgapp.di.DaggerAppComponent;

public class App extends Application {
    public static App INSTANCE;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder().build();
        }

        return appComponent;
    }
}

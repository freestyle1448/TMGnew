package tmg.crm.tmgapp.di;

import javax.inject.Singleton;

import dagger.Component;
import tmg.crm.tmgapp.di.module.NavigationModule;
import tmg.crm.tmgapp.presentation.ui.MainActivity;

@Singleton
@Component(modules = {
        NavigationModule.class
})
public interface AppComponent {
    void inject(MainActivity activity);
}

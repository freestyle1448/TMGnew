package tmg.crm.tmgapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by eltgm on 14.02.18.
 */

class NewContractorMessage {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("agent")
    @Expose
    private Contractor agent;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Contractor getAgent() {
        return agent;
    }

    public void setAgent(Contractor agent) {
        this.agent = agent;
    }
}

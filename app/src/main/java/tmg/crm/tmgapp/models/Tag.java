package tmg.crm.tmgapp.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Tag implements Parcelable {

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };
    @SerializedName("outgo")
    @Expose
    private Boolean outgo;

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("artist")
    @Expose
    private Boolean artist;
    @SerializedName("__v")
    @Expose
    private String __v;
    @SerializedName("associations")
    @Expose
    private Associations.AssociationsList associations;

    private Tag(Parcel in) {
        id = in.readString();
        name = in.readString();
        byte tmpArtist = in.readByte();
        artist = tmpArtist == 0 ? null : tmpArtist == 1;
    }

    public Tag() {

    }


    @NonNull
    public Boolean getOutgo() {
        return this.getName() != null && this.getName().equals("TMG");
    }

    public void setOutgo(Boolean outgo) {
        this.outgo = outgo;
    }


    public Associations.AssociationsList getAssociations() {
        return associations;
    }

    public void setAssociations(Associations.AssociationsList associations) {
        this.associations = associations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getArtist() {
        if (artist == null)
            return false;
        return artist;
    }

    public void setArtist() {
        this.artist = true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeByte((byte) (artist == null ? 0 : artist ? 1 : 2));
    }

    private static class List extends ArrayList<Tag> {
    }
}
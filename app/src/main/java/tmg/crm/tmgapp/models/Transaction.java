package tmg.crm.tmgapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Transaction {

    public static final int RUB = 0;
    public static final int USD = 1;
    public static final int EUR = 2;

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sender")
    @Expose
    private Contractor sender;
    @SerializedName("receiver")
    @Expose
    private Contractor receiver;
    @SerializedName("sum")
    @Expose
    private Double sum;
    @SerializedName("currency")
    @Expose
    private Integer currency;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = new ArrayList<>();
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("unix")
    @Expose
    private String unix;

    private List<String> stringTags = new ArrayList<>();
    private Tag artist;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Contractor getSender() {
        return sender;
    }

    public void setSender(Contractor sender) {
        this.sender = sender;
    }

    public Contractor getReceiver() {
        return receiver;
    }

    public void setReceiver(Contractor receiver) {
        this.receiver = receiver;
    }


    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean compareTags(List<String> categories) {

        if (categories.size() == 0)
            return true;
        for (Tag tag :
                this.tags) {
            for (String catName :
                    categories) {
                if (tag.getName().equals(catName))
                    return true;
            }
        }


        return false;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUnix() {
        return unix;
    }

    public void setUnix(String unix) {
        this.unix = unix;
    }

    public List<String> getStringTags() {
        return stringTags;
    }

    public void setStringTags(List<String> stringTags) {
        this.stringTags = stringTags;
    }

    public Tag getArtist() {
        return artist;
    }

    public void setArtist(Tag artist) {
        this.artist = artist;
    }

    private static class TransactionList extends ArrayList<Transaction> {
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Transaction && this.getName().equals(((Transaction) obj).getName()) && this.getDate().equals(((Transaction) obj).getDate());
    }
}
package tmg.crm.tmgapp.models;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class TransactionFilter {
    private Date dateStart = new Date(1);
    private Date dateEnd = new Date(99999999999999L);
    private String artistName = "";
    private List<String> categories = new ArrayList<>();
    private String transactionName = "";
    private String sender = "";
    private String recipient = "";
    private String allFilter = "";

    private TransactionFilter() {

    }

    public static boolean isContainsTag(List<Tag> tags, String cs) {
        List<Tag> t = new ArrayList<>();
        tags.stream().filter(tag -> tag.getName().toLowerCase().contains(cs.toLowerCase())).forEach(t::add);

        return t.size() > 0;
    }

    public static List<Date> dateParser(String date) {
        if (date != null)
            if (date.equals(""))
                return null;
        List<Date> datesList = new ArrayList<>();
        String regEx = "[^[0-3]*[0-9].[0-1]*[1-9].[0-9]{4}]";
        Pattern pattern = Pattern.compile(regEx);


        String[] dates = pattern.split(date);
        String dateStart = dates[0];
        String dateEnd = dates[3];

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");

        try {
            Date startDate = format.parse(dateStart);
            Date endDate = format.parse(dateEnd);
            datesList.add(startDate);
            datesList.add(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return datesList;
    }

    public static Builder newBuilder() {
        return new TransactionFilter().new Builder();
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAllFilter() {
        return allFilter;
    }

    public List<String> getCategories() {
        return categories;
    }

    private void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public class Builder {
        private Builder() {
        }

        public Builder setDateStart(Date date) {
            TransactionFilter.this.dateStart = date;
            return this;
        }

        public Builder setDateEnd(Date date) {
            TransactionFilter.this.dateEnd = date;
            return this;
        }

        public Builder setArtistName(String name) {
            TransactionFilter.this.artistName = name;
            return this;
        }

        public Builder setCategories(List<String> categories) {
            TransactionFilter.this.setCategories(categories);
            return this;
        }

        public Builder setTransactionName(String name) {
            TransactionFilter.this.transactionName = name;
            return this;
        }

        public Builder setSender(String sender) {
            TransactionFilter.this.sender = sender;
            return this;
        }

        public Builder setRecipient(String recipient) {
            TransactionFilter.this.recipient = recipient;
            return this;
        }

        public Builder setAllFilter(String allFilter) {
            TransactionFilter.this.allFilter = allFilter;
            return this;
        }

        public TransactionFilter build() {
            return TransactionFilter.this;
        }
    }
}

package tmg.crm.tmgapp.presentation.ui;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import tmg.crm.tmgapp.models.Tag;

public interface MainView extends MvpView {
    int TRANSACTION_TAB_POSITION = 0;
    int STATISTIC_TAB_POSITION = 1;

    void highlightTab(int position);

    void showError(String msg);

    void initMenu(List<Tag> artists);
}

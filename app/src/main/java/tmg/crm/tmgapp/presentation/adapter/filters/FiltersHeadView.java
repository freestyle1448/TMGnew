package tmg.crm.tmgapp.presentation.adapter.filters;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.Collapse;
import com.mindorks.placeholderview.annotations.expand.Expand;
import com.mindorks.placeholderview.annotations.expand.Parent;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;
import com.mindorks.placeholderview.annotations.expand.SingleTop;
import com.mindorks.placeholderview.annotations.expand.Toggle;

import java.util.Calendar;
import java.util.List;

import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.models.TransactionFilter;
import tmg.crm.tmgapp.presentation.presenter.TransactionPresenter;

@Parent
@SingleTop
@Layout(R.layout.filter_heading)
public class FiltersHeadView {

    private static boolean isExpand = false;
    private final Context mContext;
    private final String mHeading;


    @View(R.id.headingTxt)
    private TextView headingTxt;
    @View(R.id.toggleIcon)
    private ImageView toggleIcon;
    @Toggle(R.id.toggleView)
    private LinearLayout toggleView;
    @ParentPosition
    private int mParentPosition;

    public FiltersHeadView(Context context, String heading) {
        mContext = context;
        mHeading = heading;
    }

    @Resolve
    private void onResolved() {
        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_18dp, null));
        headingTxt.setText(mHeading);
    }

    @Expand
    private void onExpand() {
        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_18dp, null));
        isExpand = true;
    }

    @Collapse
    public void onCollapse() {
        toggleIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_18dp, null));
        isExpand = false;
    }


    @Layout(R.layout.filters)
    public static class FiltersView {
        final Context mContext;
        private final FragmentManager fragmentManager;
        private final String dateText = "";
        private final String senderText = "Отправитель";
        private final String recipientText = "Получатель";
        private final TransactionPresenter mTransactionPresenter;
        @View(R.id.etDateFrom)
        private EditText dateFrom;
        @View(R.id.mainRecipient)
        private Spinner recipientSpinner;
        @View(R.id.mainSender)
        private Spinner senderSpinner;
        @View(R.id.tvReset)
        private TextView cancel;
        @View(R.id.tvAccept)
        private TextView add;
        @ParentPosition
        private int mParentPosition;
        @ChildPosition
        private int mChildPosition;
        private int myYear = 2018;
        private int myMonth = 1;
        private int myDay = 1;
        private SpinnerAdapter senderAdapter;
        private SpinnerAdapter recipientAdapter;

        public FiltersView(FragmentManager fragmentManager, Context context, TransactionPresenter mTransactionPresenter) {
            this.fragmentManager = fragmentManager;
            mContext = context;
            this.mTransactionPresenter = mTransactionPresenter;
        }

        public void setRecipientAdapter(SpinnerAdapter recipientAdapter) {
            this.recipientAdapter = recipientAdapter;
        }


        public void setSenderAdapter(SpinnerAdapter senderAdapter) {
            this.senderAdapter = senderAdapter;
        }

        @Resolve
        public void onResolved() {
            recipientSpinner.setTooltipText(recipientText);
            senderSpinner.setTooltipText(senderText);

            recipientSpinner.setAdapter(recipientAdapter);
            senderSpinner.setAdapter(senderAdapter);
        }

        public boolean isExpand() {
            return isExpand;
        }

        @Click(R.id.etDateFrom)
        private void onClick() {
            android.view.View v = dateFrom;
            Calendar now = Calendar.getInstance();
            @SuppressLint("DefaultLocale") com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                    (datePickerDialog, i, i1, i2, i3, i4, i5) -> ((EditText) v).setText(String.format("%d.%d.%d - %d.%d.%d", i2, i1 + 1, i, i5, i4 + 1, i3)),
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );

            dpd.show(fragmentManager, "Datepickerdialog");
        }


        @Click(R.id.tvReset)
        private void onResetClick() {
            dateFrom.setText("");
            recipientSpinner.setSelection(0);
            senderSpinner.setSelection(0);
        }

        public String getDate() {
            return dateText;
        }

        public String getSenderSpinner() {
            return senderText;
        }

        public String getRecipientSpinner() {
            return recipientText;
        }

        @Click(R.id.tvAccept)
        private void onAcceptClick() {
            mTransactionPresenter.getTransactions(new DisposableObserver<List<Transaction>>() {
                                                      @Override
                                                      public void onNext(List<Transaction> transactions) {

                                                      }

                                                      @Override
                                                      public void onError(Throwable e) {

                                                      }

                                                      @Override
                                                      public void onComplete() {

                                                      }
                                                  },
                    TransactionFilter.newBuilder().build());
        }
    }
}

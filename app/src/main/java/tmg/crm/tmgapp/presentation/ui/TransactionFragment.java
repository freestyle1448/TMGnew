package tmg.crm.tmgapp.presentation.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Contractor;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.models.TransactionFilter;
import tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters.AllFiltersAdapter;
import tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters.TransactionAdapter;
import tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters.TransactionsDiffUtilCallback;
import tmg.crm.tmgapp.presentation.adapter.filters.FiltersHeadView;
import tmg.crm.tmgapp.presentation.adapter.filters.SpinnerAdapter;
import tmg.crm.tmgapp.presentation.presenter.TransactionPresenter;
import tmg.crm.tmgapp.presentation.ui.util.DelayAutoCompleteTextView;

import static tmg.crm.tmgapp.presentation.ui.util.Screens.ALL_TRANSACTIONS;
import static tmg.crm.tmgapp.presentation.ui.util.Screens.TMG_TRANSACTIONS;

public class TransactionFragment extends MvpFragment implements TransactionView {
    public static final String TAG = "TransactionFragment";
    private static final String SCREEN_TAG = "screen_tag";

    @InjectPresenter
    TransactionPresenter mTransactionPresenter;
    @BindView(R.id.phvFilters)
    ExpandablePlaceHolderView mExpandableView;
    @BindView(R.id.rvTransaction)
    RecyclerView recyclerView;
    @BindView(R.id.allFilter)
    DelayAutoCompleteTextView allFilterTextView;
    private Unbinder unbinder;
    private TransactionAdapter adapter;

    FiltersHeadView.FiltersView filtersView;

    public static TransactionFragment newInstance(String screenTag) {
        TransactionFragment fragment = new TransactionFragment();

        Bundle args = new Bundle();
        args.putString(SCREEN_TAG, screenTag);
        fragment.setArguments(args);

        return fragment;
    }

    @ProvidePresenter
    TransactionPresenter provideRepositoryPresenter() {
        assert getArguments() != null;
        return new TransactionPresenter(getArguments().getString(SCREEN_TAG));
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    private void initView() {
        // RecyclerView
        adapter = new TransactionAdapter(getContext());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                llm.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(llm);

        assert getArguments() != null;
        assert getActivity() != null;
        getActivity().setTitle(getArguments().getString(SCREEN_TAG, TMG_TRANSACTIONS));

        AllFiltersAdapter allFiltersAdapter = new AllFiltersAdapter();
        allFiltersAdapter.setFilter(new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults filterResults = new FilterResults();

                if (charSequence != null) {
                    mTransactionPresenter.getTransactions(new DisposableObserver<List<Transaction>>() {
                        @Override
                        public void onNext(List<Transaction> transactions) {
                            updateData(transactions);
                        }

                        @Override
                        public void onError(Throwable e) {
                            showError(e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    }, TransactionFilter.newBuilder()
                            .setTransactionName(getArguments().getString(SCREEN_TAG))
                            .setAllFilter(charSequence.toString())
                            .build());
                } else {
                    mTransactionPresenter.getTransactions(new DisposableObserver<List<Transaction>>() {
                        @Override
                        public void onNext(List<Transaction> transactions) {
                            initData(transactions);
                        }

                        @Override
                        public void onError(Throwable e) {
                            showError(e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    }, TransactionFilter.newBuilder().setTransactionName(getArguments().getString(SCREEN_TAG)).build());
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            }
        });

        allFilterTextView.setThreshold(0);
        allFilterTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        allFilterTextView.setAdapter(allFiltersAdapter);
        switch (getArguments().getString(SCREEN_TAG)) {
            case ALL_TRANSACTIONS:
                allFilterTextView.setText(MainActivity.allTrFilter);
                break;
            case TMG_TRANSACTIONS:
                allFilterTextView.setText(MainActivity.TMGFilter);
                break;
        }


        filtersView = new FiltersHeadView.FiltersView(getFragmentManager(), getContext(), mTransactionPresenter);


        mExpandableView.addView(new FiltersHeadView(getContext(), getString(R.string.filters)));
        mExpandableView.addChildView(0, filtersView);


        mTransactionPresenter.getContractors(new DisposableObserver<List<Contractor>>() {
            @Override
            public void onNext(List<Contractor> contractors) {
                SpinnerAdapter adapter = new SpinnerAdapter(getContext(), contractors);
                filtersView.setRecipientAdapter(adapter);
                filtersView.setSenderAdapter(adapter);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
        mTransactionPresenter.getTransactions(new DisposableObserver<List<Transaction>>() {
            @Override
            public void onNext(List<Transaction> transactions) {
                initData(transactions);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }, TransactionFilter.newBuilder()
                .setTransactionName(getArguments().getString(SCREEN_TAG))
                .setAllFilter(allFilterTextView.getText().toString())
                .build());
    }

    @Override
    public void onDestroyView() {
        switch (getArguments().getString(SCREEN_TAG)) {
            case ALL_TRANSACTIONS:
                MainActivity.allTrFilter = allFilterTextView.getText().toString();
                break;
            case TMG_TRANSACTIONS:
                MainActivity.TMGFilter = allFilterTextView.getText().toString();
                break;
        }

        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void initData(List<Transaction> transactions) {
        adapter.setTransactions(transactions);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateData(List<Transaction> transactions) {
        TransactionsDiffUtilCallback productDiffUtilCallback =
                new TransactionsDiffUtilCallback(adapter.getTransactions(), transactions);
        DiffUtil.DiffResult productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback);

        adapter.setTransactions(transactions);
        productDiffResult.dispatchUpdatesTo(adapter);
    }

    @Override
    public void showError(String mes) {
        Toast.makeText(getContext(), mes, Toast.LENGTH_SHORT).show();
    }
}

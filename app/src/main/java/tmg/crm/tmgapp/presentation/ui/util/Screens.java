package tmg.crm.tmgapp.presentation.ui.util;

public class Screens {
    public static final String ALL_TRANSACTIONS = "Все транзакции";
    public static final String TMG_TRANSACTIONS = "TMG";
    public static final String AGENTS_TRANSACTIONS = "Контрагенты";
    public static final String STATISTIC = "Статистика";
}

package tmg.crm.tmgapp.presentation.ui;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import tmg.crm.tmgapp.models.Transaction;

public interface TransactionView extends MvpView {
    void initData(List<Transaction> transactions);

    void updateData(List<Transaction> transactions);

    void showError(String mes);
}

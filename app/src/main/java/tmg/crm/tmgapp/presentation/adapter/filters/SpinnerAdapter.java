package tmg.crm.tmgapp.presentation.adapter.filters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Contractor;

public class SpinnerAdapter extends ArrayAdapter<Contractor> {
    private final LayoutInflater mInflater;
    private final int mResource;
    private final List<Contractor> items;

    public SpinnerAdapter(@NonNull Context context, List<Contractor> contractors) {
        super(context, R.layout.support_simple_spinner_dropdown_item, contractors);

        mInflater = LayoutInflater.from(context);
        mResource = R.layout.support_simple_spinner_dropdown_item;
        items = contractors;
    }

    public void setData(List<Contractor> contractors, String hint) {
        Contractor contractor = new Contractor();
        contractor.setName(hint);

        List<Contractor> c = new ArrayList<>();
        c.add(contractor);
        c.addAll(contractors);
        this.clear();
        this.addAll(c);
        notifyDataSetChanged();
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        View v = createItemView(position, convertView, parent);

        return v;
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public int getPosition(@Nullable Contractor item) {
        return super.getPosition(item);
    }

    public List<Contractor> getItems() {
        return items;
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = mInflater.inflate(mResource, parent, false);

        TextView offTypeTv = convertView.findViewById(R.id.contractorName);


        Contractor contractor = items.get(position);
        ((TextView) convertView).setTextColor(getContext().getResources().getColor(R.color.lightGray, null));
        ((TextView) convertView).setTextSize(18);
        ((TextView) convertView).setText(contractor.getName());

        return convertView;
    }
}

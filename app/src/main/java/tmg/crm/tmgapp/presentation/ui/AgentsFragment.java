package tmg.crm.tmgapp.presentation.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.presentation.presenter.AgentsPresenter;

public class AgentsFragment extends MvpFragment implements AgentsView {
    public static final String TAG = "AgentsFragment";
    @InjectPresenter
    AgentsPresenter mAgentsPresenter;

    public static AgentsFragment newInstance() {
        AgentsFragment fragment = new AgentsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.agents, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}

package tmg.crm.tmgapp.presentation.presenter;


import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.domain.transactions.TransactionsInteractor;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.models.TransactionFilter;
import tmg.crm.tmgapp.presentation.ui.StatisticView;

@InjectViewState
public class StatisticPresenter extends BasePresenter<StatisticView> {
    private final TransactionsInteractor transactionsInteractor;

    public StatisticPresenter() {
        this.transactionsInteractor = new TransactionsInteractor();
    }

    public void getCachedTransactions() {
        TransactionFilter filter = TransactionFilter.newBuilder().build();
        transactionsInteractor.getTransactions(new TransactionsObserver(),filter );
    }

    @Override
    public void disconnect() {
        transactionsInteractor.dispose();
    }

    private final class TransactionsObserver extends DisposableObserver<List<Transaction>> {

        @Override
        public void onNext(List<Transaction> transactions) {
            getViewState().showStat(transactions);
        }

        @Override
        public void onError(Throwable e) {
            getViewState().showError(e.getMessage());
        }

        @Override
        public void onComplete() {

        }
    }
}

package tmg.crm.tmgapp.presentation.ui;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import tmg.crm.tmgapp.models.Transaction;

public interface StatisticView extends MvpView {
    void showError(String mes);
    void showStat(List<Transaction> transactions);
}

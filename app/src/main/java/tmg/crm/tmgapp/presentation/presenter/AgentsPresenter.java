package tmg.crm.tmgapp.presentation.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import tmg.crm.tmgapp.presentation.ui.AgentsView;

@InjectViewState
public class AgentsPresenter extends MvpPresenter<AgentsView> {

}

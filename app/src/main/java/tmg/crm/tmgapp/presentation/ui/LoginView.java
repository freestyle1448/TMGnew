package tmg.crm.tmgapp.presentation.ui;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface LoginView extends MvpView {
    void enterApp(String username);

    void showError(String msg);
}

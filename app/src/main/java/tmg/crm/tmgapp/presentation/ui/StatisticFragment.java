package tmg.crm.tmgapp.presentation.ui;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.presentation.presenter.StatisticPresenter;

import static tmg.crm.tmgapp.models.Transaction.EUR;
import static tmg.crm.tmgapp.models.Transaction.RUB;
import static tmg.crm.tmgapp.models.Transaction.USD;

public class StatisticFragment extends MvpFragment implements StatisticView {
    public static final String TAG = "StatisticFragment";
    private static final String SCREEN_TAG = "screen_tag";
    private static final int DOLLAR_RATE = 67;
    private static final int EURO_RATE = 76;
    private final long normalization = 1514775600;//TODO курсы поменять

    @InjectPresenter
    StatisticPresenter mStatisticPresenter;

    private Unbinder unbinder;

    @BindView(R.id.chart)
    LineChart chart;
    @BindView(R.id.tvTransactionAmount)
    TextView transactionAmount;
    @BindView(R.id.tvAmount)
    TextView amount;


    public static StatisticFragment newInstance(String screenTag) {
        StatisticFragment fragment = new StatisticFragment();

        Bundle args = new Bundle();
        args.putString("screen_tag", screenTag);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stat_frag, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    private void initView() {

        mStatisticPresenter.getCachedTransactions();
        //Toast.makeText(getContext(), getScreenTag() + "stat", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public String getScreenTag() {
        return getArguments().getString(SCREEN_TAG);
    }

    @Override
    public void showError(String mes) {
        Toast.makeText(getContext(), mes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showStat(List<Transaction> transactions) {

        Date maxDate = new Date(100);
        Date minDate = new Date();
        int received = 0;
        int spent = 0;
        int perSpent = 0;
        List<Entry> entries = new ArrayList<>();
        int count = 0;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat();
        double m = 0;
        long first = 0;
        if (transactions != null) {
            for (Transaction tr :
                    transactions) {

                format.applyPattern("yyyy-MM-dd");
                Date docDate = null;
                try {
                    docDate = format.parse(tr.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (maxDate.before(docDate))
                    maxDate = docDate;
                else if (minDate.after(docDate))
                    minDate = docDate;
            }
            double dif = maxDate.getTime() - minDate.getTime();
            m = dif / 2592000000.0;
        }

        if (transactions.size() > 0)
            first = Long.valueOf(transactions.get(0).getUnix());
        for (Transaction tr :
                transactions) {
            if (tr.getCurrency() != null)
                if (tr.getType() == 0) {
                    float d = (float) (Long.valueOf(tr.getUnix()) - normalization);
                    switch (tr.getCurrency()) {
                        case RUB:
                            spent += tr.getSum();
                            perSpent += tr.getSum();
                            break;
                        case USD:
                            spent += tr.getSum() * DOLLAR_RATE;
                            perSpent += tr.getSum() * DOLLAR_RATE;
                            break;
                        case EUR:
                            spent += tr.getSum() * EURO_RATE;
                            perSpent += tr.getSum() * EURO_RATE;
                            break;
                    }
                    if (m > 1.3) {
                        long DIFFERENT_MONTH = 2592000;
                        if ((first - Long.valueOf(tr.getUnix())) >= DIFFERENT_MONTH) {
                            entries.add(new Entry(d, perSpent));
                            perSpent = 0;
                            first = Long.valueOf(tr.getUnix());

                        }
                    } else if (m > 0.9) {
                        long DIFFERENT_WEEK = 604800;
                        if ((first - Long.valueOf(tr.getUnix())) >= DIFFERENT_WEEK) {
                            entries.add(new Entry(d, perSpent));
                            perSpent = 0;
                            first = Long.valueOf(tr.getUnix());
                        }
                    } else {
                        long q = first - Long.valueOf(tr.getUnix());
                        long DIFFERENT_DAY = 86400;
                        if ((first - Long.valueOf(tr.getUnix()) >= DIFFERENT_DAY)) {
                            entries.add(new Entry(d, perSpent));
                            perSpent = 0;
                            first = Long.valueOf(tr.getUnix());
                        } else {
                            entries.add(new Entry(d, perSpent));
                            perSpent = 0;
                        }
                    }
                } else {
                    switch (tr.getCurrency()) {
                        case RUB:
                            received += tr.getSum();
                            break;
                        case USD:
                            received += tr.getSum() * DOLLAR_RATE;
                            break;
                        case EUR:
                            received += tr.getSum() * EURO_RATE;
                            break;
                    }
                }
            count++;
        }

        LineDataSet dataSet;
        entries.sort(new EntryXComparator());
        if (entries.size() > 0) {
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_red);
            dataSet = new LineDataSet(entries, "Label");
            dataSet.setDrawValues(false);
            dataSet.setDrawFilled(true);
            dataSet.setFillDrawable(drawable);

            LineData lineData = new LineData(dataSet);
            chart.setData(lineData);
        }
        chart.getDescription().setEnabled(false);


        YAxis y = chart.getAxisLeft();
        y.setGranularityEnabled(true);
        y.setGranularity(5000f);


        chart.animateY(1500);  //1.5 сек
        chart.setTouchEnabled(false); //касани не распознаются графиком
        chart.setDrawGridBackground(true); //заливка фона графика

        chart.getXAxis().setDrawGridLines(false); // убираем вертикальные линии
        chart.getAxisRight().setEnabled(false);
        Legend l = chart.getLegend();
        l.setEnabled(false);
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setCenterAxisLabels(true);
        xAxis.setGranularity(43200f); // one hour
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private final SimpleDateFormat mFormat = new SimpleDateFormat("dd-MM", Locale.UK);

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                long s = ((long) value) + normalization;
                Date n = new Date(s * 1000);
                String sq = mFormat.format(n);
                return mFormat.format(n);
            }
        });

        //chart.setDescription(description)

        chart.invalidate();

        transactionAmount.setText(String.format(Locale.ROOT, "Всего транзакций: %d", transactions.size()));


        amount.setText(String.format(Locale.US, "\u20BD %,d", spent));
    }
}

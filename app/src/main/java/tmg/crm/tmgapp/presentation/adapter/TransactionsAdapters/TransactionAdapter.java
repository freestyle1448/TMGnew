package tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Tag;
import tmg.crm.tmgapp.models.Transaction;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private final Context context;
    private List<Transaction> transactions;

    public TransactionAdapter(Context context) {
        this.context = context;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.transaction_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Transaction transaction = transactions.get(position);

        viewHolder.setTransactionName(transaction.getName());

        viewHolder.setSender((transaction.getSender()).getName());
        viewHolder.setRecipient((transaction.getReceiver()).getName());
        viewHolder.setDate(transaction.getDate());
        String currency = null;
        switch (transaction.getCurrency()) {
            case 0:
                currency = "\u20BD";
                break;
            case 1:
                currency = "$";
                break;
            case 2:
                currency = "€";
                break;
        }
        viewHolder.setAmount((int) Math.round(transaction.getSum()), currency);
        viewHolder.setTags(transaction.getTags());
    }

    @Override
    public int getItemCount() {
        if (transactions != null)
            return transactions.size();
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTransaction;
        // private final TextView tvArtist;
        private final TextView tvSender;
        private final TextView tvRecipient;
        private final TextView tvAmount;
        private final RecyclerView rvTags;
        private final TextView tvDate;

        ViewHolder(View itemView) {
            super(itemView);
            this.tvTransaction = itemView.findViewById(R.id.tvTransactionName);
            //this.tvArtist = itemView.findViewById(R.id.tvArtist);
            this.tvSender = itemView.findViewById(R.id.tvSender);
            this.tvRecipient = itemView.findViewById(R.id.tvRecipient);
            this.tvAmount = itemView.findViewById(R.id.tvAmount);
            this.rvTags = itemView.findViewById(R.id.rvTags);
            this.tvDate = itemView.findViewById(R.id.tvDate);

            LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rvTags.setLayoutManager(llm);

        }

        void setTransactionName(String transactionName) {
            tvTransaction.setText(transactionName);
        }

        // public void setArtist(String artist) {
        //tvArtist.setText(artist);
        //}

        void setSender(String sender) {
            tvSender.setText(sender);
        }

        void setRecipient(String recipient) {
            tvRecipient.setText(recipient);
        }

        @SuppressLint("DefaultLocale")
        void setAmount(double amount, String currency) {

            tvAmount.setText(String.format("%s%,d", currency, Math.round(amount)));
        }

        void setTags(List<Tag> tags) {
            rvTags.setAdapter(new HorizontalAdapter(tags));
        }

        void setDate(String date) {
            String[] time = date.split("T");
            String d = time[0];
            this.tvDate.setText(d);
        }
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private final List<Tag> horizontalList;


        HorizontalAdapter(List<Tag> horizontalList) {
            this.horizontalList = horizontalList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tag_item_trans, parent, false);
            final MyViewHolder h = new MyViewHolder(itemView);
            itemView.setOnClickListener(v -> {
                //TODO Обработка клика по тагам
                int adapterPos = h.getAdapterPosition();
            });
            return h;
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
            holder.tag.setText(horizontalList.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            final TextView tag;

            MyViewHolder(View view) {
                super(view);
                tag = view.findViewById(R.id.tvTag);
            }
        }
    }
}

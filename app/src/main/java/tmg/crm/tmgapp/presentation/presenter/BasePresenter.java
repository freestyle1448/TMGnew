package tmg.crm.tmgapp.presentation.presenter;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

abstract class BasePresenter<View extends MvpView> extends MvpPresenter<View> {
    @Override
    public void onDestroy() {
        this.disconnect();
        super.onDestroy();
    }

    protected abstract void disconnect();
}

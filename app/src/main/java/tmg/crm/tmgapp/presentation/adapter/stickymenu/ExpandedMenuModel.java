package tmg.crm.tmgapp.presentation.adapter.stickymenu;

public class ExpandedMenuModel {

    private String iconName = "";
    private int iconImg = -1; // menu icon resource id

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getIconImg() {
        return iconImg;
    }

    public void setIconImg() {
        this.iconImg = tmg.crm.tmgapp.R.drawable.ic_person_black_24dp;
    }
}
package tmg.crm.tmgapp.presentation.adapter.stickymenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import tmg.crm.tmgapp.R;

/**
 * Created by eltgm on 12.02.18.
 */

public class StickyListAdapter extends BaseAdapter implements StickyListHeadersAdapter {


    private final List<Entity> entities = new ArrayList<>();
    private final LayoutInflater inflater;
    private final Context context;

    public StickyListAdapter(Context context) {

        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setData(ExpandedMenuModel head, List<String> childs) {
        if (entities.size() > 0)
            entities.clear();
        for (String s :
                childs) {
            entities.add(new Entity(s, head));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (entities == null)
            return 0;
        return entities.size();
    }


    @Override
    public Object getItem(int position) {
        return entities.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.menu_artist, parent, false);
            holder.text = convertView.findViewById(R.id.submenu);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(entities.get(position).getChild());

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.menu_artist_header, parent, false);
            holder.text = convertView.findViewById(R.id.submenu);
            holder.imageView = convertView.findViewById(R.id.iconimage);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name

        holder.text.setText(entities.get(position).getHeader().getIconName());
        holder.imageView.setImageDrawable(context.getDrawable(entities.get(position).getHeader().getIconImg()));

        return convertView;
    }


    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        return entities.get(position).getHeader().getIconImg();
    }


    class HeaderViewHolder {
        TextView text;
        ImageView imageView;
    }

    class ViewHolder {
        TextView text;
    }

    public class Entity {
        final String child;
        final ExpandedMenuModel header;

        Entity(String child, ExpandedMenuModel header) {
            this.child = child;
            this.header = header;
        }

        public String getChild() {
            return child;
        }

        ExpandedMenuModel getHeader() {
            return header;
        }
    }
}
package tmg.crm.tmgapp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.domain.contractors.ContractorsInteractor;
import tmg.crm.tmgapp.domain.transactions.TransactionsInteractor;
import tmg.crm.tmgapp.models.Contractor;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.models.TransactionFilter;
import tmg.crm.tmgapp.presentation.ui.TransactionView;

@InjectViewState
public class TransactionPresenter extends BasePresenter<TransactionView> {
    private final ContractorsInteractor contractorsInteractor;
    private final TransactionsInteractor transactionsInteractor;
    private final String screenTag;

    public TransactionPresenter(String screenTag) {
        this.contractorsInteractor = new ContractorsInteractor();
        this.transactionsInteractor = new TransactionsInteractor();
        this.screenTag = screenTag;
    }

    public void getTransactions(DisposableObserver<List<Transaction>> observer, TransactionFilter filter) {
        transactionsInteractor.getTransactions(observer, filter);
    }

    public void getContractors(DisposableObserver<List<Contractor>> observer) {
        contractorsInteractor.getContractors(observer);
    }

    @Override
    public void disconnect() {
        transactionsInteractor.dispose();
    }

    public final class TransactionsObserverForUpdate extends DisposableObserver<List<Transaction>> {

        @Override
        public void onNext(List<Transaction> transactions) {
            getViewState().updateData(transactions);
        }

        @Override
        public void onError(Throwable e) {
            getViewState().showError(e.getMessage());
        }

        @Override
        public void onComplete() {

        }
    }
}

package tmg.crm.tmgapp.presentation.ui;

import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.presentation.presenter.LoginPresenter;

public class LoginActivity extends MvpAppCompatActivity implements LoginView {
    @InjectPresenter
    LoginPresenter loginPresenter;

    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.bLogin)
    Button bLogin;
    @BindView(R.id.layout)
    View layout;

    @OnClick(R.id.bLogin)
    void onLoginClick() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password))
            loginPresenter.onAuthClick(username, password);
        else if (TextUtils.isEmpty(username)) {
            etUsername.setError("Введите имя пользователя!");
            etUsername.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            etPassword.setError("Введите пароль!");
            etPassword.requestFocus();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
    }

    private void initViews() {
        ButterKnife.bind(this);

        etPassword.setImeActionLabel("Done", EditorInfo.IME_ACTION_DONE);
        etPassword.setOnEditorActionListener((textView, i, keyEvent) -> {
            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();

            if (i == EditorInfo.IME_ACTION_DONE) {
                if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                    loginPresenter.onAuthClick(username, password);
                    return true;
                } else if (TextUtils.isEmpty(username)) {
                    etUsername.setError("Введите имя пользователя!");
                    etUsername.requestFocus();
                    return true;
                } else if (TextUtils.isEmpty(password)) {
                    etPassword.setError("Введите пароль!");
                    etPassword.requestFocus();
                    return true;
                }
            }

            return false;
        });
    }

    @Override
    public void enterApp(String username) {
        startActivity(MainActivity.getIntent(this, username));
    }

    @Override
    public void showError(String msg) {
        Snackbar.make(layout, msg, BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
    }
}

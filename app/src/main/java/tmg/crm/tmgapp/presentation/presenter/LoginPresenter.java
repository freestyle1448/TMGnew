package tmg.crm.tmgapp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;

import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.domain.login.LoginInteractor;
import tmg.crm.tmgapp.models.Message;
import tmg.crm.tmgapp.presentation.ui.LoginView;

@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {
    private final LoginInteractor loginInteractor;

    public LoginPresenter() {
        this.loginInteractor = new LoginInteractor();
    }

    public void onAuthClick(String username, String password) {
        loginInteractor.login(new AuthObserver(username));
    }

    @Override
    public void disconnect() {
        loginInteractor.dispose();
    }

    private final class AuthObserver extends DisposableObserver<Message> {
        private final String username;

        private AuthObserver(String username) {
            this.username = username;
        }


        @Override
        public void onNext(Message message) {
            if (message.getSuccess())
                getViewState().enterApp(username);
            else
                getViewState().showError(message.getMessage());
        }

        @Override
        public void onError(Throwable e) {
            getViewState().showError(e.getMessage());
        }

        @Override
        public void onComplete() {

        }
    }

}

package tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.List;

import tmg.crm.tmgapp.models.Transaction;

public class AllFiltersAdapter extends BaseAdapter implements Filterable {

    private List<Transaction> mResults;
    private Filter filter;

    public AllFiltersAdapter() {
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}

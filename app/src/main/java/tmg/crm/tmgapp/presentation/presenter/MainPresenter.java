package tmg.crm.tmgapp.presentation.presenter;


import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import ru.terrakok.cicerone.Router;
import tmg.crm.tmgapp.domain.artists.ArtistsInteractor;
import tmg.crm.tmgapp.models.Tag;
import tmg.crm.tmgapp.presentation.ui.AgentsFragment;
import tmg.crm.tmgapp.presentation.ui.MainView;
import tmg.crm.tmgapp.presentation.ui.StatisticFragment;
import tmg.crm.tmgapp.presentation.ui.TransactionFragment;

import static tmg.crm.tmgapp.presentation.ui.util.Screens.AGENTS_TRANSACTIONS;
import static tmg.crm.tmgapp.presentation.ui.util.Screens.ALL_TRANSACTIONS;
import static tmg.crm.tmgapp.presentation.ui.util.Screens.TMG_TRANSACTIONS;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    private final ArtistsInteractor artistsInteractor;
    private final Router router;

    public MainPresenter(Router router) {
        this.artistsInteractor = new ArtistsInteractor();
        this.router = router;
    }

    @Override
    public void disconnect() {
        artistsInteractor.dispose();
    }

    public void loadArtist() {
        artistsInteractor.getArtists(new ArtistsObserver());
    }

    public void logOut() {
        router.exit();
    }

    public void onAllTransactionClicked() {
        router.navigateTo(TransactionFragment.TAG, ALL_TRANSACTIONS);
    }

    public void onTMGTransactionClicked() {
        router.navigateTo(TransactionFragment.TAG, TMG_TRANSACTIONS);
    }

    public void onAgentsTransactionClicked() {
        router.navigateTo(AgentsFragment.TAG, AGENTS_TRANSACTIONS);
    }

    public void onTabTransactionClick(String transactionScreenTag) {
        getViewState().highlightTab(MainView.TRANSACTION_TAB_POSITION);
        router.navigateTo(TransactionFragment.TAG, transactionScreenTag);
    }

    public void onTabStatisticClick(String transactionScreenTag) {
        getViewState().highlightTab(MainView.STATISTIC_TAB_POSITION);
        router.navigateTo(StatisticFragment.TAG, transactionScreenTag);
    }

    private final class ArtistsObserver extends DisposableObserver<List<Tag>> {

        @Override
        public void onNext(List<Tag> artists) {
            getViewState().initMenu(artists);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }
}

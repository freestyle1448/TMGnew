package tmg.crm.tmgapp.presentation.adapter.TransactionsAdapters;

import android.support.v7.util.DiffUtil;

import java.util.List;

import tmg.crm.tmgapp.models.Transaction;

public class TransactionsDiffUtilCallback extends DiffUtil.Callback {
    private final List<Transaction> oldList;
    private final List<Transaction> newList;

    public TransactionsDiffUtilCallback(List<Transaction> oldList, List<Transaction> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Transaction oldTransaction = oldList.get(oldItemPosition);
        Transaction newTransaction = newList.get(newItemPosition);
        return oldTransaction.getId().equals(newTransaction.getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Transaction oldTransaction = oldList.get(oldItemPosition);
        Transaction newTransaction = newList.get(newItemPosition);


        return oldTransaction.getDate().equals(newTransaction.getDate()) && oldTransaction.getSum().equals(newTransaction.getSum());
    }
}

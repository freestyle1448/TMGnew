package tmg.crm.tmgapp.presentation.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.FragmentNavigator;
import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import tmg.crm.tmgapp.App;
import tmg.crm.tmgapp.R;
import tmg.crm.tmgapp.models.Tag;
import tmg.crm.tmgapp.presentation.adapter.stickymenu.ExpandedMenuModel;
import tmg.crm.tmgapp.presentation.adapter.stickymenu.StickyListAdapter;
import tmg.crm.tmgapp.presentation.presenter.MainPresenter;
import tmg.crm.tmgapp.presentation.ui.util.RouterProvider;

import static android.view.View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS;

public class MainActivity extends MvpAppCompatActivity implements MainView, NavigationView.OnNavigationItemSelectedListener, RouterProvider {
    public static final String TAG = "MainActivity";

    public static String allTrFilter = "";
    public static String TMGFilter = "";
    public static String artistFilter = "";

    @BindView(R.id.bottomNav)
    BottomNavigationBar bottomNavigationView;

    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    Router router;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @InjectPresenter
    MainPresenter mMainPresenter;
    @BindView(R.id.drawer_layout)
    View layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String SCREEN_TAG;
    private ExpandableStickyListHeadersListView expandableList;
    private final Navigator navigator = new FragmentNavigator(getFragmentManager(), R.id.flMain) {
        @Override
        protected android.app.Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case TransactionFragment.TAG:
                    SCREEN_TAG = (String) data;
                    return TransactionFragment.newInstance((String) data);
                case AgentsFragment.TAG:
                    SCREEN_TAG = "Контрагенты";
                    toolbar.setTitle("Контрагенты");
                    return AgentsFragment.newInstance();
                case StatisticFragment.TAG:
                    return StatisticFragment.newInstance(SCREEN_TAG);
            }
            return null;
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finishAndRemoveTask();
        }
    };

    public static Intent getIntent(final Context context, final String username) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("username", username);
        return intent;
    }

    @ProvidePresenter
    public MainPresenter createMainPresenter() {
        return new MainPresenter(router);
    }

    private void initLog() {

    }

    private void initView() {
        ButterKnife.bind(this);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        bottomNavigationView
                .addItem(new BottomNavigationItem(R.drawable.ic_attach_money_black_24dp, R.string.transaction))
                .addItem(new BottomNavigationItem(R.drawable.ic_equalizer_black_24dp, R.string.stat))
                .initialise();
        bottomNavigationView.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                switch (position) {
                    case TRANSACTION_TAB_POSITION:
                        mMainPresenter.onTabTransactionClick(SCREEN_TAG);
                        break;
                    case STATISTIC_TAB_POSITION:
                        mMainPresenter.onTabStatisticClick(SCREEN_TAG);
                }
            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {
                onTabSelected(position);
            }
        });


        mMainPresenter.loadArtist();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLog();
        initView();
    }

    private int idGenerator() {
        Random random = new Random();
        return random.nextInt();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.head_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_exit:
                mMainPresenter.logOut();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void highlightTab(int position) {
        bottomNavigationView.selectTab(position, false);
    }

    @Override
    public void showError(String msg) {
        Snackbar.make(layout, msg, BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
    }

    public void initMenu(List<Tag> artists) {
        View headerLayout = navigationView.getHeaderView(0);
        ImageView userPhoto = headerLayout.findViewById(R.id.userPhoto);
        userPhoto.setImageDrawable(getDrawable(R.drawable.logo));

        navigationView.setNavigationItemSelectedListener(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {

            }

            @Override
            public void onDrawerOpened(@NonNull View view) {

            }

            @Override
            public void onDrawerClosed(@NonNull View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        expandableList = findViewById(R.id.navigationmenu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            expandableList.setImportantForAutofill(IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }
        expandableList.setDivider(null);
        StickyListAdapter mMenuAdapter = new StickyListAdapter(this);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);
        Menu menu = navigationView.getMenu();
        menu.getItem(0).setChecked(true);//TODO перенести в навигацию

        expandableList.setOnHeaderClickListener((l, header, itemPosition, headerId, currentlySticky) -> {
            if (expandableList.isHeaderCollapsed(headerId)) {
                expandableList.expand(headerId);
            } else {
                expandableList.collapse(headerId);
            }
        });

        expandableList.setOnItemClickListener((parent, view, position, id) -> {
            Menu menu1 = navigationView.getMenu();
            for (int j = 0; j < menu1.size(); j++) {
                menu1.getItem(j).setChecked(false);
            }

            switch (position) {
                case 0:
                    Snackbar.make(view, "Добавить", Snackbar.LENGTH_LONG).show();
                    break;
                default:
                    menu1.getItem(position).setChecked(true);
                    StickyListAdapter.Entity s = (StickyListAdapter.Entity) mMenuAdapter.getItem(position);
                    String artistName = s.getChild();
                    Snackbar.make(view, artistName, Snackbar.LENGTH_LONG).show();
                    break;
            }
        });

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIconName(getString(R.string.all_artist));
        item1.setIconImg();

        // Adding child data
        List<String> heading1 = new ArrayList<>();
        heading1.add(getString(R.string.add_artist));

        for (Tag tag
                : artists) {
            heading1.add(tag.getName());
        }

        mMenuAdapter.setData(item1, heading1);


        mMainPresenter.onAllTransactionClicked();

        if (bottomNavigationView.getVisibility() == View.GONE)
            bottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.general:
                mMainPresenter.onAllTransactionClicked();

                highlightTab(TRANSACTION_TAB_POSITION);
                if (bottomNavigationView.getVisibility() == View.GONE)
                    bottomNavigationView.setVisibility(View.VISIBLE);
                break;
            case R.id.internalCosts:
                mMainPresenter.onTMGTransactionClicked();

                highlightTab(TRANSACTION_TAB_POSITION);
                if (bottomNavigationView.getVisibility() == View.GONE)
                    bottomNavigationView.setVisibility(View.VISIBLE);
                break;
            case R.id.agents:
                mMainPresenter.onAgentsTransactionClicked();
                bottomNavigationView.setVisibility(View.GONE);
                break;
        }

        ((DrawerLayout) layout).closeDrawers();

        return true;
    }

    @Override
    public Router getRouter() {
        return router;
    }
}

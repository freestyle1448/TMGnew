package tmg.crm.tmgapp.presentation.ui.util;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {
    Router getRouter();
}

package tmg.crm.tmgapp.domain.login;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.models.Message;

public class LoginInteractor {

    private final CompositeDisposable disposables;

    public LoginInteractor() {
        this.disposables = new CompositeDisposable();
    }

    public void dispose() {
        if (!disposables.isDisposed())
            disposables.clear();
    }

    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }


    public void login(DisposableObserver<Message> observer) {
        Message m = new Message();
        m.setMessage("Пошёл нахуй ишак ");
        m.setSuccess(true);
        addDisposable(Observable.just(m).subscribeWith(observer));
    }
}

package tmg.crm.tmgapp.domain.transactions;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import tmg.crm.tmgapp.models.Contractor;
import tmg.crm.tmgapp.models.Tag;
import tmg.crm.tmgapp.models.Transaction;
import tmg.crm.tmgapp.models.TransactionFilter;

import static tmg.crm.tmgapp.models.TransactionFilter.isContainsTag;

public class TransactionsInteractor {
    private final CompositeDisposable disposables;

    public TransactionsInteractor() {
        this.disposables = new CompositeDisposable();
    }

    public void dispose() {
        if (!disposables.isDisposed())
            disposables.clear();
    }

    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    public void getTransactions(DisposableObserver<List<Transaction>> observer, TransactionFilter filter) {
        Contractor receiver = new Contractor();
        receiver.setId("1");
        receiver.setName("Отправитель Отправитель");
        Contractor sender = new Contractor();
        sender.setId("2");
        sender.setName("Получаетль Получатель");
        Tag artist = new Tag();
        artist.setArtist();
        artist.setId("1");
        artist.setName("Тестовый артист");
        Transaction transaction = new Transaction();
        transaction.setArtist(artist);
        transaction.setCurrency(0);
        transaction.setDate("2018-07-27T16:31:30.110Z");
        transaction.setId("01");
        transaction.setName("Тестовая транзакция");
        transaction.setReceiver(receiver);
        transaction.setSender(sender);
        transaction.setUnix("1533888804");
        transaction.setSum(5000000.5000);
        List<Transaction> transactionList = new ArrayList<>(10);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);

        Contractor contractor = new Contractor();
        contractor.setId("1");
        contractor.setName("Отправитель Отправитель1");
        Contractor sender1 = new Contractor();
        sender1.setId("21");
        sender1.setName("Получаетль Получатель11");
        Tag artist1 = new Tag();
        artist1.setArtist();
        artist1.setId("11");
        artist1.setName("Тестовый артист1");
        Transaction transaction2 = new Transaction();
        transaction2.setArtist(artist1);
        transaction2.setCurrency(0);
        transaction2.setDate("2018-07-27T16:31:30.110Z");
        transaction2.setId("01");
        transaction2.setName("Тестовая транзакция 1");
        transaction2.setReceiver(contractor);
        transaction2.setSender(sender1);
        transaction2.setUnix("1533888804");
        transaction2.setSum(123321.11);

        transactionList.add(transaction2);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //noinspection unchecked
        addDisposable(Observable.fromArray(transactionList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(transactions -> {
                    List<Transaction> newTransactions = new ArrayList<>();
                    Supplier<Stream<Transaction>> transactionStream = transactions::stream;

                    transactionStream.get().filter(transaction1 -> {
                        try {
                            String[] dates = transaction1.getDate().split("T");
                            String newDate = dates[0] + " " + dates[1].substring(0, dates[1].length() - 1);
                            return (mFormat.parse(newDate)).after(filter.getDateStart()) && (mFormat.parse(newDate)).before(filter.getDateEnd());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }).forEach(newTransactions::add);

                    if (!filter.getRecipient().equals("Получатель") && !filter.getRecipient().equals(""))
                        newTransactions.removeIf(transaction1 -> !transaction1.getReceiver().getName().equals(filter.getRecipient()));

                    if (!filter.getSender().equals("Отправитель") && !filter.getSender().equals(""))
                        newTransactions.removeIf(transaction1 -> !transaction1.getSender().getName().equals(filter.getSender()));

                    if ((!filter.getRecipient().equals("Получатель") && !filter.getSender().equals("Отправитель")) && (!filter.getRecipient().equals("") && !filter.getSender().equals("")))
                        newTransactions.removeIf(transaction1 -> !(transaction1.getSender().getName().equals(filter.getSender()) && transaction1.getReceiver().getName().equals(filter.getRecipient())));

                    if (filter.getAllFilter() != null && !TextUtils.isEmpty(filter.getAllFilter())) {
                        newTransactions.removeIf(transaction1 ->
                                !isContainsTag(transaction1.getTags(), filter.getAllFilter())
                                        && !transaction1.getName().toLowerCase().contains(filter.getAllFilter().toLowerCase()));
                    }

                    if (!filter.getTransactionName().equals("") && !filter.getTransactionName().equals("Все транзакции")) {
                        if (filter.getTransactionName().equals("TMG"))
                            newTransactions.removeIf(transaction12 -> !isContainsTag(transaction12.getTags(), "TMG"));
                    }

                    return newTransactions;
                })
                .subscribeWith(observer));
    }
}

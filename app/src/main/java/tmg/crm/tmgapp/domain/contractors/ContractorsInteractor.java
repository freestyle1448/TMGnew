package tmg.crm.tmgapp.domain.contractors;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.models.Contractor;

public class ContractorsInteractor {
    private final CompositeDisposable disposables;

    public ContractorsInteractor() {
        this.disposables = new CompositeDisposable();
    }

    public void dispose() {
        if (!disposables.isDisposed())
            disposables.clear();
    }

    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    public void getContractors(DisposableObserver<List<Contractor>> observer) {
        List<Contractor> contractors = new ArrayList<>();
        Contractor contractor = Contractor.builder()
                .name("Отправ1")
                .id("1")
                .build();
        Contractor contractor2 = Contractor.builder()
                .name("Отправ2")
                .id("2")
                .build();
        Contractor contractor3 = Contractor.builder()
                .name("Отправ33")
                .id("3")
                .build();

        contractors.add(contractor);
        contractors.add(contractor2);
        contractors.add(contractor3);

        //noinspection unchecked
        addDisposable(Observable.fromArray(contractors).subscribeWith(observer));
    }
}

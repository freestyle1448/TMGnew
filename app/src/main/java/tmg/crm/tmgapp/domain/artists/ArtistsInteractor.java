package tmg.crm.tmgapp.domain.artists;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import tmg.crm.tmgapp.models.Tag;

public class ArtistsInteractor {
    private final CompositeDisposable disposables;

    public ArtistsInteractor() {
        this.disposables = new CompositeDisposable();
    }

    public void dispose() {
        if (!disposables.isDisposed())
            disposables.clear();
    }

    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    public void getArtists(DisposableObserver<List<Tag>> observer) {
        Tag tag = new Tag();
        tag.setArtist();
        tag.setName("Test1");
        Tag tag1 = new Tag();
        tag1.setArtist();
        tag1.setName("Test2");
        Tag tag2 = new Tag();
        tag2.setArtist();
        tag2.setName("Test3");
        Tag tag3 = new Tag();
        tag3.setArtist();
        tag3.setName("Test4");
        Tag tag4 = new Tag();
        tag4.setArtist();
        tag4.setName("Test5");
        //TODO удалить тест

        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        tags.add(tag1);
        tags.add(tag2);
        tags.add(tag3);
        tags.add(tag4);

        //noinspection unchecked
        addDisposable(Observable.fromArray(tags).subscribeWith(observer));
    }
}
